/*******************************************************************************
* COPYRIGHT NOTICE
* Copyright (c) 2018,逐飞科技
* All rights reserved.
*
* 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
* 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
*
* @file			zf_common_headfile
* @company		成都逐飞科技有限公司
* @author		逐飞科技(QQ3184284598)
* @version		查看doc内version文件 版本说明
* @Software		IAR 8.32.4 or MDK 5.28
* @Target core	MM32SPIN27PS
* @Taobao		https://seekfree.taobao.com/
* @date			2022-02-12
*******************************************************************************/

#ifndef _zf_common_headfile_h_
#define _zf_common_headfile_h_

#include "stdio.h"
#include "stdint.h"
#include "string.h"

//=================================芯片 SDK 底层=================================
#include "HAL_conf.h"
#include "reg_common.h"
#include "reg_adc.h"
#include "reg_dma.h"
#include "reg_exti.h"
#include "reg_flash.h"
#include "reg_gpio.h"
#include "reg_i2c.h"
#include "reg_rcc.h"
#include "reg_spi.h"
#include "reg_tim.h"
#include "reg_uart.h"
//=================================芯片 SDK 底层=================================

//====================================================开源库公共层====================================================
#include "zf_common_typedef.h"
#include "zf_common_debug.h"
#include "zf_common_fifo.h"
#include "zf_common_font.h"
#include "zf_common_function.h"
#include "zf_common_interrupt.h"
//====================================================开源库公共层====================================================

//===================================================芯片外设驱动层===================================================
#include "zf_driver_adc.h"
#include "zf_device_camera.h"
#include "zf_driver_exti.h"
#include "zf_driver_flash.h"
#include "zf_driver_gpio.h"
#include "zf_driver_soft_iic.h"
#include "zf_driver_pit.h"
#include "zf_driver_pwm.h"
#include "zf_driver_spi.h"
#include "zf_driver_delay.h"
#include "zf_driver_timer.h"
#include "zf_driver_uart.h"
//===================================================芯片外设驱动层===================================================

//===================================================外接设备驱动层===================================================
#include "zf_device_absolute_encoder.h"
#include "zf_device_icm20602.h"
#include "zf_device_ips114_spi.h"
#include "zf_device_l3g4200d.h"
#include "zf_device_mma8451.h"
#include "zf_device_mpu6050.h"
#include "zf_device_mt9v03x.h"
#include "zf_device_oled.h"
#include "zf_device_ov7725.h"
#include "zf_device_tft180.h"
#include "zf_device_tsl1401.h"
#include "zf_device_uart_7725.h"
#include "zf_device_virsco.h"
#include "zf_device_wireless.h"
//===================================================外接设备驱动层===================================================
//====Hardware====
#include "led.h"
#include "adc.h"
#include "timer_pwm.h"
#include "pwm_input.h"
#include "opamp.h"
#include "comp.h"
//====BLDC====
#include "hall.h"
#include "pid.h"
#include "motor.h"
#include "move_filter.h"


#endif

