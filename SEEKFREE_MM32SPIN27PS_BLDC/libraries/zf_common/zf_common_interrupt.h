/*******************************************************************************
* COPYRIGHT NOTICE
* Copyright (c) 2018,逐飞科技
* All rights reserved.
*
* 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
* 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
*
* @file			zf_common_interrupt
* @company		成都逐飞科技有限公司
* @author		逐飞科技(QQ3184284598)
* @version		查看doc内version文件 版本说明
* @Software		IAR 8.32.4 or MDK 5.28
* @Target core	MM32SPIN27PS
* @Taobao		https://seekfree.taobao.com/
* @date			2022-02-12
*******************************************************************************/

#ifndef _zf_common_interrupt_h_
#define _zf_common_interrupt_h_

#include "hal_misc.h"
#include "zf_common_typedef.h"

void nvic_init(IRQn_Type irqn, uint8 priority, FunctionalState status);
void nvic_interrput_enable (void);
void nvic_interrput_disable (void);

void	NMI_Handler				(void);
void	HardFault_Handler		(void);
void	MemManage_Handler		(void);
void	BusFault_Handler		(void);
void	UsageFault_Handler		(void);
void	SVC_Handler				(void);
void	DebugMon_Handler		(void);
void	PendSV_Handler			(void);
void	SysTick_Handler			(void);


#endif
