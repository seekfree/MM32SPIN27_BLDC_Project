/*******************************************************************************
* COPYRIGHT NOTICE
* Copyright (c) 2018,逐飞科技
* All rights reserved.
*
* 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
* 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
*
* @file			zf_common_debug
* @company		成都逐飞科技有限公司
* @author		逐飞科技(QQ3184284598)
* @version		查看doc内version文件 版本说明
* @Software		IAR 8.32.4 or MDK 5.28
* @Target core	MM32SPIN27PS
* @Taobao		https://seekfree.taobao.com/
* @date			2022-02-12
*******************************************************************************/

#ifndef _zf_common_debug_h_
#define _zf_common_debug_h_

#include "zf_common_typedef.h"

#define DEBUG_UART_INDEX            UART_1                                      // 指定 debug 所使用的的串口
#define DEBUG_UART_BAUDRATE         115200                                      // 指定 debug 所使用的的串口波特率
#define DEBUG_UART_TX_PIN           UART1_TX_B09                                 // 指定 debug 所使用的的串口引脚
#define DEBUG_UART_RX_PIN           UART1_RX_B08                                // 指定 debug 所使用的的串口引脚

#define DEBUG_UART_USE_INTERRUPT    1                                           // 是否启用 debug 串口接收中断
#if DEBUG_UART_USE_INTERRUPT                                                    // 如果启用 debug 串口接收中断
#define DEBUG_RING_BUFFER_LEN       64                                          // 定义环形缓冲区大小 默认 64byte

extern uint8  debug_ring_buffer[DEBUG_RING_BUFFER_LEN];                        // 环形缓冲区
extern uint32 debug_ring_index;                                                // 缓冲区下标

void        debug_interrupr_handler (void);
uint32      debug_read_ring_buffer  (uint8 *data);
#endif

void debug_init (void);

#endif
