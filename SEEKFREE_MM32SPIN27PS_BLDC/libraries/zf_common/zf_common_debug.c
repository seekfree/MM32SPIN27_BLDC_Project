/*******************************************************************************
* COPYRIGHT NOTICE
* Copyright (c) 2018,逐飞科技
* All rights reserved.
*
* 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
* 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
*
* @file			zf_common_debug
* @company		成都逐飞科技有限公司
* @author		逐飞科技(QQ3184284598)
* @version		查看doc内version文件 版本说明
* @Software		IAR 8.32.4 or MDK 5.28
* @Target core	MM32SPIN27PS
* @Taobao		https://seekfree.taobao.com/
* @date			2022-02-12
*******************************************************************************/

#include "zf_common_debug.h"
#include "zf_common_interrupt.h"
#include "zf_driver_uart.h"

uint8 zf_debug_init_flag = 0;

//-------------------------------------------------------------------------------------------------------------------
// @brief       debug 串口初始化
// @param       void
// @return      void
// Sample usage:    debug_init();
//-------------------------------------------------------------------------------------------------------------------
void debug_init (void)
{
    uart_init(
        DEBUG_UART_INDEX,                                                       // 在 zf_common_debug.h 中查看对应值
        DEBUG_UART_BAUDRATE,                                                    // 在 zf_common_debug.h 中查看对应值
        DEBUG_UART_TX_PIN,                                                      // 在 zf_common_debug.h 中查看对应值
        DEBUG_UART_RX_PIN);                                                     // 在 zf_common_debug.h 中查看对应值
	if(DEBUG_UART_USE_INTERRUPT)                                                // 如果启用串口中断
    {
        uart_rx_irq(DEBUG_UART_INDEX, 1);                                 // 使能对应串口接收中断
    }
	zf_debug_init_flag = 1;
}

#if DEBUG_UART_USE_INTERRUPT                                                   // 条件编译 只有在启用串口中断才编译

uint8  debug_ring_buffer[DEBUG_RING_BUFFER_LEN];                                // 创建环形缓冲区
uint32 debug_ring_index = 0;                                                    // 环形缓冲区下标索引

//-------------------------------------------------------------------------------------------------------------------
// @brief       debug 串口中断处理函数 isr.c 中对应串口中断服务函数调用
// @param       void
// @return      void
// Sample usage:            debug_interrupr_handler();
//-------------------------------------------------------------------------------------------------------------------
void debug_interrupr_handler (void)
{
    uart_query(DEBUG_UART_INDEX, &debug_ring_buffer[debug_ring_index++]);  // 读取数据到环形缓冲区
    if(debug_ring_index == DEBUG_RING_BUFFER_LEN)   debug_ring_index = 0;       // 环形缓冲区收尾衔接
}

//-------------------------------------------------------------------------------------------------------------------
// @brief       读取 debug 环形缓冲区数据
// @param       *data       读出数据存放的数组指针
// @return      uint32    读出数据长度
// Sample usage:            uint8 data[64]; len = debug_read_ring_buffer(data);
//-------------------------------------------------------------------------------------------------------------------
uint32 debug_read_ring_buffer (uint8 *data)
{
    if(debug_ring_index == 0)   return 0;
    uint32 temp = debug_ring_index;                                             // 获取数据长度
    memcpy(data, debug_ring_buffer, temp);                                      // 拷贝数据
    debug_ring_index = 0;                                                       // 清空下标
    return temp;
}
#endif



//--------------------------------------------------------------------------------				// printf 重定向 此部分不允许用户更改
#ifdef __GNUC__
#define PUTCHAR_PROTOTYPE s32 __io_putchar(s32 ch)
#else
#define PUTCHAR_PROTOTYPE s32 fputc(s32 ch, FILE *f)
#endif

#ifdef USE_IAR
PUTCHAR_PROTOTYPE {
	while((UART1->CSR & UART_CSR_TXC) == 0); //The loop is sent until it is finished
	UART1->TDR = (ch & (u16)0x00FF);
	return ch;
}
#else
s32 fputc(s32 ch, FILE* f)
{
	while((UART1->CSR & UART_CSR_TXC) == 0); //The loop is sent until it is finished
	UART1->TDR = (ch & (u16)0x00FF);
	return ch;
}

#endif
//--------------------------------------------------------------------------------			// printf 重定向 此部分不允许用户更改
