/*******************************************************************************
* COPYRIGHT NOTICE
* Copyright (c) 2018,逐飞科技
* All rights reserved.
*
* 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
* 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
*
* @file			zf_driver_pit
* @company		成都逐飞科技有限公司
* @author		逐飞科技(QQ3184284598)
* @version		查看doc内version文件 版本说明
* @Software		IAR 8.32.4 or MDK 5.28
* @Target core	MM32SPIN27PS
* @Taobao		https://seekfree.taobao.com/
* @date			2022-02-12
*******************************************************************************/

#ifndef _zf_driver_pit_h
#define _zf_driver_pit_h

#include "zf_common_typedef.h"
 
#include "zf_driver_timer.h"

void pit_init (tim_enum tim, uint32 freq, uint8 priority);
void pit_init_us (tim_enum tim, uint32 timer, uint8 priority);
void pit_init_ms (tim_enum tim, uint32 timer, uint8 priority);
void pit_enable (tim_enum tim);
void pit_disable (tim_enum tim);

#endif
