/*******************************************************************************
* COPYRIGHT NOTICE
* Copyright (c) 2018,逐飞科技
* All rights reserved.
*
* 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
* 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
*
* @file			zf_driver_exti
* @company		成都逐飞科技有限公司
* @author		逐飞科技(QQ3184284598)
* @version		查看doc内version文件 版本说明
* @Software		IAR 8.32.4 or MDK 5.28
* @Target core	MM32SPIN27PS
* @Taobao		https://seekfree.taobao.com/
* @date			2022-02-12
*******************************************************************************/

#ifndef _zf_driver_exti_h
#define _zf_driver_exti_h

#include "zf_common_typedef.h"
#include "hal_exti.h"
#include "hal_misc.h"
#include "hal_syscfg.h"
#include "zf_driver_gpio.h"

void exti_init (pin_enum pin, EXTITrigger_TypeDef trigger, uint8 priority);
void exti_enable (pin_enum pin);
void exti_disable (pin_enum pin);
void exti_even_init (pin_enum pin, EXTITrigger_TypeDef trigger);
void exti_even_enable (pin_enum pin);
void exti_even_disable (pin_enum pin);

#endif
