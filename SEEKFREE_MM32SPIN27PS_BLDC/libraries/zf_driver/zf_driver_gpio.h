/*******************************************************************************
* COPYRIGHT NOTICE
* Copyright (c) 2018,逐飞科技
* All rights reserved.
*
* 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
* 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
*
* @file			zf_driver_gpio
* @company		成都逐飞科技有限公司
* @author		逐飞科技(QQ3184284598)
* @version		查看doc内version文件 版本说明
* @Software		IAR 8.32.4 or MDK 5.28
* @Target core	MM32SPIN27PS
* @Taobao		https://seekfree.taobao.com/
* @date			2022-02-12
*******************************************************************************/

#ifndef _zf_driver_gpio_h
#define _zf_driver_gpio_h

#include "zf_common_typedef.h"
#include "hal_gpio.h"

typedef enum //枚举端口方向
{
	A0 = 0x00,	A1 = 0x01,	A2 = 0x02,	A3 = 0x03,	A4 = 0x04,	A5 = 0x05,	A6 = 0x06,	A7 = 0x07,
	A8 = 0x08,	A9 = 0x09,	A10 = 0x0A,	A11 = 0x0B,	A12 = 0x0C,	A13 = 0x0D,	A14 = 0x0E,	A15 = 0x0F,

	B0 = 0x10,	B1 = 0x11,	B2 = 0x12,	B3 = 0x13,	B4 = 0x14,	B5 = 0x15,	B6 = 0x16,	B7 = 0x17,
	B8 = 0x18,	B9 = 0x19,	B10 = 0x1A,	B11 = 0x1B,	B12 = 0x1C,	B13 = 0x1D,	B14 = 0x1E,	B15 = 0x1F,

	C0 = 0x20,	C1 = 0x21,	C2 = 0x22,	C3 = 0x23,	C4 = 0x24,	C5 = 0x25,	C6 = 0x26,	C7 = 0x27,
	C8 = 0x28,	C9 = 0x29,	C10 = 0x2A,	C11 = 0x2B,	C12 = 0x2C,	C13 = 0x2D,	C14 = 0x2E,	C15 = 0x2F,

	D0 = 0x30,	D1 = 0x31,	D2 = 0x32,	D3 = 0x33,	D4 = 0x34,	D5 = 0x35,	D6 = 0x36,	D7 = 0x37,
}pin_enum;


typedef enum																					// 枚举端口方向
{
	GPI = 0x00,																					// 定义管脚输入
	GPO = 0x03,																					// 定义管脚输出
}gpio_dir_enum;

typedef enum																					// 枚举端口方向
{
	GPI_ANAOG_IN		= 0x00,																	// 定义管脚模拟输入
	GPI_FLOATING_IN		= 0x04,																	// 定义管脚浮空输入
	GPI_PULL_DOWN		= 0x08,																	// 定义管脚下拉输入
	GPI_PULL_UP			= 0x08,																	// 定义管脚上拉输入

	GPO_PUSH_PULL		= 0x00,																	// 定义管脚推挽输出
	GPO_OPEN_DTAIN		= 0x04,																	// 定义管脚开漏输出
	GPO_AF_PUSH_PUL		= 0x08,																	// 定义管脚复用推挽输出
	GPO_AF_OPEN_DTAIN	= 0x0C,																	// 定义管脚复用开漏输出
}gpio_mode_enum;

typedef enum																					// 枚举端口复用功能
{
	GPIO_AF0 = 0x00,																			// 引脚复用功能选项 0
	GPIO_AF1 = 0x01,																			// 引脚复用功能选项 1
	GPIO_AF2 = 0x02,																			// 引脚复用功能选项 2
	GPIO_AF3 = 0x03,																			// 引脚复用功能选项 3
	GPIO_AF4 = 0x04,																			// 引脚复用功能选项 4
	GPIO_AF5 = 0x05,																			// 引脚复用功能选项 5
	GPIO_AF6 = 0x06,																			// 引脚复用功能选项 6
	GPIO_AF7 = 0x07,																			// 引脚复用功能选项 7
}gpio_af_enum;

typedef enum																					// 枚举端口电平
{
	GPIO_LOW = 0,																				// 定义管脚默认电平
	GPIO_HIGH = 1,																				// 定义管脚默认电平
}gpio_level_enum;

#define GPIO_PIN_RESET(x)	gpio_group[(x>>4)]->BRR  |= ((uint16_t)0x0001 << (x & 0x0F))		//GPIO复位
#define GPIO_PIN_SET(x)		gpio_group[(x>>4)]->BSRR  |= ((uint16_t)0x0001 << (x & 0x0F))		//GPIO置位

extern GPIO_TypeDef *gpio_group[4];

void		gpio_init		(pin_enum pin, gpio_dir_enum dir, uint8 dat, gpio_mode_enum mode);
void		afio_init		(pin_enum pin, gpio_dir_enum dir, gpio_af_enum af, gpio_mode_enum mode);
void		gpio_set		(pin_enum pin, uint8 dat);
uint8		gpio_get		(pin_enum pin);
void		gpio_dir		(pin_enum pin, gpio_dir_enum dir, gpio_mode_enum mode);
void		gpio_toggle		(pin_enum pin);

#endif
