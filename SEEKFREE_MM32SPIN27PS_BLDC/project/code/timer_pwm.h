/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2021,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：联系淘宝客服
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file            adc
 * @company         成都逐飞科技有限公司
 * @author          逐飞科技(QQ3184284598)
 * @version         查看doc内version文件 版本说明
 * @Software		IAR 8.32.4 or MDK 5.28
 * @Target core		MM32SPIN27PS
 * @Taobao          https://seekfree.taobao.com/
 * @date            2021-3-14
 ********************************************************************************************************************/
#ifndef _TIMER_PWM_H
#define _TIMER_PWM_H

#include "HAL_conf.h"
#include "zf_driver_pwm.h"


#define FCY  ((uint32)96000000)                    	    //系统时钟
#define FPWM  ((uint16)40000)                       	//PWM频率
#define PWM_PRIOD_LOAD (uint16)(FCY/FPWM/2)    	        //PWM周期装载值
#define DEADTIME_LOAD (9)	         			        //死区装载值

#define TIM3_PRIOD    1                          		//定时器3周期值

void tim1_complementary_pwm(uint16 period,uint8 dead_time);
void tim1_complementary_pwm_control(uint8 status);
void tim3_init(uint16 prescaler, uint16 period);

#endif
