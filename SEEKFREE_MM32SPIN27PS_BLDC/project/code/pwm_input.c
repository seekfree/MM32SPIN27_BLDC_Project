/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2021,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：联系淘宝客服
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file            adc
 * @company         成都逐飞科技有限公司
 * @author          逐飞科技(QQ3184284598)
 * @version         查看doc内version文件 版本说明
 * @Software		IAR 8.32.4 or MDK 5.28
 * @Target core		MM32SPIN27PS
 * @Taobao          https://seekfree.taobao.com/
 * @date            2021-3-14
 ********************************************************************************************************************/

#include "pwm_input.h"


float duty_cycle;
int16 period, duty_low, duty_high;


void pwm_input_init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_AHBPeriphClockCMD(RCC_AHBPeriph_GPIOC, ENABLE);
    GPIO_PinAFConfig(MOTOR_PWM_PORT, GPIO_PinSource9, GPIO_AF_3);
    GPIO_InitStructure.GPIO_Pin = MOTOR_PWM_PIN;      //PWM输入捕获引脚
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
    GPIO_Init(MOTOR_PWM_PORT, &GPIO_InitStructure);
    
	gpio_init(MOTOR_DIR_PORT, GPI, 0, GPI_PULL_DOWN);
    
    
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = TIM8_CC_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelCMD = ENABLE; //IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);                 //根据指定的参数初始化NVIC寄存器
    
    NVIC_InitStructure.NVIC_IRQChannel = TIM8_BRK_UP_TRG_COM_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelCMD = ENABLE; //IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);                 //根据指定的参数初始化NVIC寄存器
	
	
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    TIM_ICInitTypeDef       TIM8_ICInitStructure;

    RCC_APB2PeriphClockCMD(RCC_APB2Periph_TIM8, ENABLE);

    TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
    TIM_TimeBaseStructure.TIM_Period = 20000;
    TIM_TimeBaseStructure.TIM_Prescaler = SystemCoreClock / 1000000 - 1;
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM8, &TIM_TimeBaseStructure);

    TIM8_ICInitStructure.TIM_Channel = TIM_Channel_3;   //上升沿通道
    TIM8_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Falling;
    TIM8_ICInitStructure.TIM_ICSelection = TIM_ICSelection_IndirectTI;
    TIM8_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
    TIM8_ICInitStructure.TIM_ICFilter = 10;
    TIM_ICInit(TIM8, &TIM8_ICInitStructure);

    TIM_ICStructInit(&TIM8_ICInitStructure);
    TIM8_ICInitStructure.TIM_Channel = TIM_Channel_4;   //下降沿通道
    TIM8_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;
    TIM8_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
    TIM8_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
    TIM8_ICInitStructure.TIM_ICFilter = 10;
    TIM_ICInit(TIM8, &TIM8_ICInitStructure);

    TIM_ITConfig(TIM8, TIM_IT_CC4, ENABLE);             //使能下降沿通道中断
    TIM_ITConfig(TIM8, TIM_IT_Update, ENABLE);          //使能更新中断
    TIM_ClearITPendingBit(TIM8, TIM_IT_CC4);
    TIM_ClearITPendingBit(TIM8, TIM_IT_Update);

    TIM_CMD(TIM8, ENABLE);
}

uint8 pwm_dir_get(void)
{
    //获取输入速度的正负极性
    //0：期望电机正转  1：期望电机反转
    return gpio_get(MOTOR_DIR_PORT);
}

void pwm_input_trigger_callback(void)
{
	float duty;
	period = TIM_GetCapture4(TIM8);       //获取周期值
	duty_high = TIM_GetCapture3(TIM8);    //获取高电平时间

	duty = (float)duty_high/period;
	
	// 限制捕获的占空比为5%-95%
	if(duty < 0.05)
	{
		duty = 0.05;
	}
	else if(duty > 0.90)
	{
		duty = 0.90;
	}
	
	// 将5%-95%
	// 处理为0%-90%
	duty = duty - 0.05;

	//根据接收到的信号，计算方向
	if(pwm_dir_get() == 1)
	{
		duty = -duty;
	}
	
	
	duty_cycle = duty;
}

void pwm_input_timeout_callback(void)
{
	period = 2000;
	duty_low = 0;
	duty_high = 0;
	duty_cycle = 0;
}








