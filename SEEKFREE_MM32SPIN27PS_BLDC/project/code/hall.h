/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2021,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：联系淘宝客服
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file            adc
 * @company         成都逐飞科技有限公司
 * @author          逐飞科技(QQ3184284598)
 * @version         查看doc内version文件 版本说明
 * @Software		IAR 8.32.4 or MDK 5.28
 * @Target core		MM32SPIN27PS
 * @Taobao          https://seekfree.taobao.com/
 * @date            2021-3-14
 ********************************************************************************************************************/
#ifndef _hall_H
#define _hall_H

#include "HAL_conf.h"

#include "zf_common_function.h"
#include "zf_driver_gpio.h"

#include "move_filter.h"
#include "motor.h"
#include "led.h"
#include "timer_pwm.h"


//定义电机极对数
#define	POLEPAIRS		    7

//定义一分钟内ADC中断次数
#define ADC_NUM_PM          60*FPWM

#define COMMUTATION_TIMEOUT 10000

#define HALL_A_PIN          A0
#define HALL_B_PIN          A1
#define HALL_C_PIN          A2



extern uint8 hall_value_now; //当前霍尔的值
extern uint8 hall_value_last;//上次霍尔的值

extern uint8 next_hall_value;

extern uint32 commutation_delay;
extern uint16 commutation_time;
extern uint32 commutation_time_sum;

extern void scan_hall_status(void);
extern void read_hall_value(void);
extern void hall_init(void);
#endif
