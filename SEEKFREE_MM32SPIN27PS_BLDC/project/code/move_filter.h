/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2021,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：联系淘宝客服
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file            adc
 * @company         成都逐飞科技有限公司
 * @author          逐飞科技(QQ3184284598)
 * @version         查看doc内version文件 版本说明
 * @Software		IAR 8.32.4 or MDK 5.28
 * @Target core		MM32SPIN27PS
 * @Taobao          https://seekfree.taobao.com/
 * @date            2021-3-14
 ********************************************************************************************************************/
#ifndef _move_filter_h
#define _move_filter_h

#include "zf_common_typedef.h"

#include "motor.h"


#define MOVE_AVERAGE_SIZE    6  //定义缓冲区大小


typedef struct
{
	uint8 index;            //下标
    uint8 buffer_size;      //buffer大小
	int32 data_buffer[MOVE_AVERAGE_SIZE];  //缓冲区
	int32 data_sum;         //数据和
	int32 data_average;     //数据平局值
}move_filter_struct;









extern move_filter_struct speed_filter;


void move_filter_init(move_filter_struct *move_average);
void move_filter_calc(move_filter_struct *move_average, int32 new_data);



#endif

