/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2021,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：联系淘宝客服
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file            adc
 * @company         成都逐飞科技有限公司
 * @author          逐飞科技(QQ3184284598)
 * @version         查看doc内version文件 版本说明
 * @Software		IAR 8.32.4 or MDK 5.28
 * @Target core		MM32SPIN27PS
 * @Taobao          https://seekfree.taobao.com/
 * @date            2021-3-14
 ********************************************************************************************************************/

#include "pid.h"


closed_loop_struct closed_loop;

//-------------------------------------------------------------------------------------------------------------------
//  @brief      PI闭环计算
//  @param      read_speed  当前速度 
//  @return     void					
//  @since      
//-------------------------------------------------------------------------------------------------------------------
void closed_loop_pi_calc(int32 read_speed)
{
    closed_loop.real_speed = read_speed;
    
    closed_loop.error = closed_loop.target_speed - closed_loop.real_speed;

    closed_loop.pout = (int32)(closed_loop.error * (closed_loop.kp + (float)myabs(closed_loop.error/1000)/1800));
    
    //积分系数根据误差进行动态调节
    closed_loop.iout += closed_loop.error * (closed_loop.ki + (float)myabs(closed_loop.error/1000)/38000);
    
    //积分限幅
    closed_loop.iout = limit_ab(closed_loop.iout, -closed_loop.out_max, closed_loop.out_max);

    //如果目标速度为0或者电机被关闭则清除积分
    if((0 == closed_loop.target_speed )|| (MOTOR_DISABLE == motor_control.en_status))
    {
        closed_loop.iout = 0;
    }
    
    //正转的时候对积分进行限幅
    if(0 < closed_loop.target_speed)
    {
        closed_loop.iout = limit_ab(closed_loop.iout, (float)closed_loop.target_speed/motor_control.max_speed*closed_loop.out_max/2, closed_loop.out_max);
    }
    //反转的时候对积分进行限幅
    else if(0 > closed_loop.target_speed)
    {
        closed_loop.iout = limit_ab(closed_loop.iout, -closed_loop.out_max, (float)closed_loop.target_speed/motor_control.max_speed*closed_loop.out_max/2);
    }

    closed_loop.out = (int32)(closed_loop.iout + closed_loop.pout);
    
    //输出限幅
    closed_loop.out = limit_ab(closed_loop.out, -closed_loop.out_max, closed_loop.out_max);
}


//-------------------------------------------------------------------------------------------------------------------
//  @brief      PI闭环计算初始化
//  @param      void   
//  @return     void					
//  @since      
//-------------------------------------------------------------------------------------------------------------------
void closed_loop_pi_init(void)
{
    closed_loop.out_max = PWM_PRIOD_LOAD;
    closed_loop.kp = 0.001;
    closed_loop.ki = 0.00001;
    closed_loop.out = 0;
    closed_loop.real_speed = 0;
}
