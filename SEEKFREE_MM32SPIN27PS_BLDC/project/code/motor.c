/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2021,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：联系淘宝客服
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file            adc
 * @company         成都逐飞科技有限公司
 * @author          逐飞科技(QQ3184284598)
 * @version         查看doc内version文件 版本说明
 * @Software		IAR 8.32.4 or MDK 5.28
 * @Target core		MM32SPIN27PS
 * @Taobao          https://seekfree.taobao.com/
 * @date            2021-3-14
 ********************************************************************************************************************/

#include "motor.h"


motor_struct motor_control;


uint16  pwm_duty_value;     // 记录速度调节需要设置的占空比，在进行电机换相的时候再对CCR寄存器进行设置

//-------------------------------------------------------------------------------------------------------------------
//  @brief      速度曲线计算函数
//  @param      void
//  @return     void					
//  @since      
//-------------------------------------------------------------------------------------------------------------------
void motor_speed_curve(void)
{
    if(FORWARD == motor_control.dir)
    {   //设置的速度比闭环的目标速度大，则使用步进值将闭环目标速度逐步逼近设置速度
        if(motor_control.set_speed > closed_loop.target_speed)
        {
            closed_loop.target_speed += motor_control.increase_step;
            if(closed_loop.target_speed > motor_control.set_speed)
            {
                closed_loop.target_speed = motor_control.set_speed;
            }
        }
        //设置的速度比目标速度低，则不使用步进值
        else
        {
            closed_loop.target_speed = motor_control.set_speed;
            if(0 > closed_loop.target_speed) 
            {
                closed_loop.target_speed = 0;
            }
        }
    }
    else//反转
    {   //设置的速度比闭环的目标速度大，则使用步进值将闭环目标速度逐步逼近设置速度
        if(motor_control.set_speed < closed_loop.target_speed)
        {
            closed_loop.target_speed -= motor_control.increase_step;
            if(closed_loop.target_speed < motor_control.set_speed)
            {
                closed_loop.target_speed = motor_control.set_speed;
            }
        }
        //设置的速度比目标速度低，则不使用步进值
        else
        {
            closed_loop.target_speed = motor_control.set_speed;
            if(0 < closed_loop.target_speed) 
            {
                closed_loop.target_speed = 0;
            }
        }
    }
    
    
    //限幅
    if(closed_loop.target_speed > motor_control.max_speed)
    {
        closed_loop.target_speed = motor_control.max_speed;
    }
    if(closed_loop.target_speed < motor_control.min_speed)
    {
        closed_loop.target_speed = motor_control.min_speed;
    }
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      设置电机转动方向
//  @param      void
//  @return     void					
//  @since      
//-------------------------------------------------------------------------------------------------------------------
void motor_set_dir(void)
{
    //当速度为0的时候才检测是否需要切换方向
    //if(speed_filter.data_average == 0)
    {
        if(!pwm_dir_get())
        {
            motor_control.dir = FORWARD;
        }
        else
        {
            motor_control.dir = REVERSE;
        }
    }
    
    //输出电机实际运行的方向信息
    motor_dir_out();
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      刹车
//  @param      void
//  @return     void					
//  @since      
//-------------------------------------------------------------------------------------------------------------------
void motor_brake(void)
{
    PWMUVWH_OFF_UVWL_ON;//如果刹车使能则下桥全开，上桥全关
    TIM1->CCR1 = PWM_PRIOD_LOAD / 100 * BLDC_BRAKE_PERCENT;
    TIM1->CCR2 = PWM_PRIOD_LOAD / 100 * BLDC_BRAKE_PERCENT;
    TIM1->CCR3 = PWM_PRIOD_LOAD / 100 * BLDC_BRAKE_PERCENT;
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      内部调用
//  @param      void
//  @return     void					
//  @since      
//-------------------------------------------------------------------------------------------------------------------
void pwm_a_bn_output(void)
{
    PWMW_Dis;
    PWMWN_Dis;
    
    PWMU_Enb;
    PWMUN_Enb;
    
    PWMV_Enb;
    PWMVN_Enb;
    
    
    TIM1->CCR1 = pwm_duty_value;
    TIM1->CCR2 = 0;
    TIM1->CCR3 = 0;
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      内部调用
//  @param      void
//  @return     void					
//  @since      
//-------------------------------------------------------------------------------------------------------------------
void pwm_a_cn_output(void)
{
    PWMV_Dis;
    PWMVN_Dis;
    
    PWMU_Enb;
    PWMUN_Enb;
    
    PWMW_Enb;
    PWMWN_Enb;
    
    TIM1->CCR1 = pwm_duty_value;
    TIM1->CCR2 = 0;
    TIM1->CCR3 = 0;
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      内部调用
//  @param      void
//  @return     void					
//  @since      
//-------------------------------------------------------------------------------------------------------------------
void pwm_b_cn_output(void)
{
    PWMU_Dis;
    PWMUN_Dis;
    
    PWMW_Enb;
    PWMWN_Enb;
    
    PWMV_Enb;
    PWMVN_Enb;

    TIM1->CCR1 = 0;
    TIM1->CCR2 = pwm_duty_value;
    TIM1->CCR3 = 0;
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      内部调用
//  @param      void
//  @return     void					
//  @since      
//-------------------------------------------------------------------------------------------------------------------
void pwm_b_an_output(void)
{
    PWMW_Dis;
    PWMWN_Dis;
    
    PWMU_Enb;
    PWMUN_Enb;
    
    PWMV_Enb;
    PWMVN_Enb;

    TIM1->CCR1 = 0;
    TIM1->CCR2 = pwm_duty_value;
    TIM1->CCR3 = 0;
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      内部调用
//  @param      void
//  @return     void					
//  @since      
//-------------------------------------------------------------------------------------------------------------------
void pwm_c_an_output(void)
{
    PWMV_Dis;
    PWMVN_Dis;
    
    PWMU_Enb;
    PWMUN_Enb;
    
    PWMW_Enb;
    PWMWN_Enb;

    TIM1->CCR1 = 0;
    TIM1->CCR2 = 0;
    TIM1->CCR3 = pwm_duty_value;
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      内部调用
//  @param      void
//  @return     void					
//  @since      
//-------------------------------------------------------------------------------------------------------------------
void pwm_c_bn_output(void)
{
    PWMUVW_Enb;
    PWMUVWN_Enb;
    
    PWMU_Dis;
    PWMUN_Dis;
    

    TIM1->CCR1 = 0;
    TIM1->CCR2 = 0;
    TIM1->CCR3 = pwm_duty_value;
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      更新占空比
//  @param      void
//  @return     void					
//  @since      
//-------------------------------------------------------------------------------------------------------------------
void motor_duty_out(int16 duty)
{
    // 这里进需要更新变量的值，最终在换相的时候修改CCR寄存器
    pwm_duty_value = myabs(duty);

}
//-------------------------------------------------------------------------------------------------------------------
//  @brief      输出动力
//  @param      void
//  @return     void					
//  @since      
//-------------------------------------------------------------------------------------------------------------------
void motor_power_out(void)
{
    int16 duty;
    duty = closed_loop.out;

    if(motor_control.dir == FORWARD)
    {//电机正转
        if(0 > duty)
        {
            duty = 0;
        }
    }
    else
    {//电机反转
        if(0 >= duty)
        {
            duty = -duty;
        }
        else
        {
            duty = 0;
        }
    }
    
    
    if(0 == closed_loop.target_speed && motor_control.brake_flag)
    {//当刹车标志位开启的时候并且设置的速度为0 此时需要进行刹车
        motor_brake();
    }
    else if(MOTOR_DISABLE == motor_control.en_status)
    {//当电机使能开关关闭的时候需要进行刹车
        motor_brake();
    }
    else
    {
        motor_duty_out(duty);
    }
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      电机换相函数
//  @param      except_hall 期望下次霍尔的值
//  @return     void					
//  @since      
//-------------------------------------------------------------------------------------------------------------------
void motor_commutation(uint8 except_hall)
{
    if(0 == closed_loop.target_speed && motor_control.brake_flag)
    {
        motor_brake();
    }
    else if(MOTOR_DISABLE == motor_control.en_status)
    {
        motor_brake();
    }
    else
    {
        switch(except_hall)
        {
            case 1:
                pwm_b_cn_output();//1
                break;
            
            case 2:		
                
                pwm_a_bn_output();//2
                break;
            
            case 3:		
                pwm_a_cn_output();//3
                
                break;
            
            case 4:	
                pwm_c_an_output();//4    
                
                break;
            
            case 5:		
                pwm_b_an_output();//5
                break;
            
            case 6:		
                pwm_c_bn_output();//6
                break;
        }
    }
    
}


//-------------------------------------------------------------------------------------------------------------------
//  @brief      电机使能状态查询
//  @param      frequency   需要设置的频率 
//  @return     void					
//  @since      
//-------------------------------------------------------------------------------------------------------------------
void tim2_pwm_frequency(uint16 frequency)
{

    if(0 == frequency)
    {
        pwm_set_duty(MOTOR_SPEED_TIMER, MOTOR_SPEED_OUT_PORT, 0);           //频率为0的时候，将占空比设置为0即可
    }
    else
    {
        pwm_set_freq(MOTOR_SPEED_TIMER, frequency);    					  //设置输出频率
        pwm_set_duty(MOTOR_SPEED_TIMER, MOTOR_SPEED_OUT_PORT, PWM_DUTY_MAX/2);    //设置占空比 
    }
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      计算电机实际运行方向
//  @void       
//  @return     void					
//  @since      
//  @note       
//-------------------------------------------------------------------------------------------------------------------
void motor_direction_calculate(void)
{
    int8 temp;
    uint8 hall_index;
    uint8 hall_index_last;
    
    const uint8 hall_value_array[7] = {0, 3, 1, 2, 5, 4, 6};
    
    hall_index_last = hall_value_array[hall_value_last];
    hall_index = hall_value_array[hall_value_now];
    
    
    temp = (int8)hall_index - (int8)hall_index_last;
    
    if(myabs(temp) > 3)
    {
        if(temp>3)
        {
            temp -= 6;
        }
        else
        {
            temp += 6;
        }
    }

    motor_control.dir_now = temp>0?FORWARD:REVERSE;
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      电机速度输出
//  @param      void 
//  @return     void					
//  @since      每次换相的时候翻转IO，外部控制器用编码器接口直接采集即可
//              速度引脚连接外部控制器编码器接口的A通道 方向引脚连接B通道
//-------------------------------------------------------------------------------------------------------------------
void motor_speed_out(void)
{
    tim2_pwm_frequency(myabs(speed_filter.data_average));
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      电机转动方向输出
//  @param      void 
//  @return     void					
//  @since      
//-------------------------------------------------------------------------------------------------------------------
void motor_dir_out(void)
{
	if(speed_filter.data_average >= 0)
	{
		gpio_set(MOTOR_DIR_OUT_PORT, Bit_RESET);
	}
	else
	{
		gpio_set(MOTOR_DIR_OUT_PORT, Bit_SET);
	}
	
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      电机使能状态查询
//  @param      void 
//  @return     void					
//  @since      
//-------------------------------------------------------------------------------------------------------------------
void motor_en(void)
{
    if(gpio_get(MOTOR_EN_STATUS_PORT))
    {
        motor_control.en_status = MOTOR_DISABLE;
    }
    else
    {
        motor_control.en_status = MOTOR_ENABLE;
    }
}



//-------------------------------------------------------------------------------------------------------------------
//  @brief      电机速度与方向输出 初始化
//  @param      void 
//  @return     void					
//  @since      
//-------------------------------------------------------------------------------------------------------------------
void motor_information_out_init(void)
{
    pwm_init(MOTOR_SPEED_TIMER, MOTOR_SPEED_OUT_PORT, 1000, 0);
	gpio_init(MOTOR_DIR_OUT_PORT, GPO, 0, GPO_PUSH_PULL);
	gpio_init(MOTOR_EN_STATUS_PORT, GPI, 0, GPI_PULL_DOWN);
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      电机速度曲线初始化
//  @param      void 
//  @return     void					
//  @since      
//-------------------------------------------------------------------------------------------------------------------
void motor_speed_curve_init(void)
{
    #if BLDC_BRAKE_ENABLE==1
        motor_control.brake_flag = 1;   //刹车使能
    #else
        motor_control.brake_flag = 0;   //刹车关闭
    #endif
    motor_control.dir = FORWARD;                    //设置默认的方向
    
    motor_control.set_speed = 0;
    #if(BLDC_CLOSE_LOOP_ENABLE)
    motor_control.max_speed = BLDC_MAX_SPEED;       //设置最大正转速度
    motor_control.min_speed = -BLDC_MAX_SPEED;      //设置最大反转速度
    motor_control.increase_step = BLDC_STEP_SPEED;  //设置加速时的步进值
    #endif
}


