/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2021,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：联系淘宝客服
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file            adc
 * @company         成都逐飞科技有限公司
 * @author          逐飞科技(QQ3184284598)
 * @version         查看doc内version文件 版本说明
 * @Software		IAR 8.32.4 or MDK 5.28
 * @Target core		MM32SPIN27PS
 * @Taobao          https://seekfree.taobao.com/
 * @date            2021-3-14
 ********************************************************************************************************************/
 
#include "timer_pwm.h"


void tim1_complementary_pwm(uint16 period,uint8 dead_time)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	TIM_BDTRInitTypeDef TIM_BDTRInitStruct;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB2PeriphClockCMD(RCC_APB2Periph_TIM1, ENABLE);
	RCC_AHBPeriphClockCMD(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB, ENABLE);

	//定时器 CH1 CH2 CH3引脚初始化 用于控制上桥
	GPIO_InitStructure.GPIO_Pin  =  GPIO_Pin_3|GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin  =  GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource15, GPIO_AF_7);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource7,  GPIO_AF_7);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6,  GPIO_AF_7);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource5,  GPIO_AF_7);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource4,  GPIO_AF_7);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource3,  GPIO_AF_7);

    TIM_DeInit(TIM1);
	
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Period = period;				                //设置定时器周期值 
	TIM_TimeBaseStructure.TIM_Prescaler = 0;						        //定时器时钟不分频
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_CenterAligned1; //中心对齐模式  PWM频率=定时器输入时钟频率/(2*周期值)
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; 				//不分频
	TIM_TimeBaseStructure.TIM_RepetitionCounter = 1;						//占空比的更新频率=定时器输入时钟频率/((TIM_RepetitionCounter+1)*周期值)
	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure); 					


	TIM_OCStructInit(&TIM_OCInitStructure); 						        
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; 						//PWM模式1，定时器计数值小于比较值的时候，引脚输出有效电平
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; 			//CHx通道使能
	TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;         //CHxN通道关闭
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; 				//CHx  有效电平为高电平 无效电平为低电平
	TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High; 			//CHxN 有效电平为高电平 无效电平为低电平
	TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset; 			//CHx  空闲状态输出低电平
	TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Reset; 			//CHxN 空闲状态输出低电平

	TIM_OCInitStructure.TIM_Pulse = 0; 						
	TIM_OC1Init(TIM1, &TIM_OCInitStructure);				
	TIM_OCInitStructure.TIM_Pulse = 0; 						
	TIM_OC2Init(TIM1, &TIM_OCInitStructure);				
	TIM_OCInitStructure.TIM_Pulse = 0; 						
	TIM_OC3Init(TIM1, &TIM_OCInitStructure);				
	TIM_OCInitStructure.TIM_Pulse = 1;      //设置通道4比较值
	TIM_OC4Init(TIM1, &TIM_OCInitStructure);//CC4用于触发ADC采集

	TIM_OC1PreloadConfig(TIM1,TIM_OCPreload_Disable );  //预装载失能
	TIM_OC2PreloadConfig(TIM1,TIM_OCPreload_Disable );  
	TIM_OC3PreloadConfig(TIM1,TIM_OCPreload_Disable );  
	TIM_OC4PreloadConfig(TIM1,TIM_OCPreload_Disable );  
    
    
	TIM1->PSC = 0x00;   // 定时器时钟 = HSI/(PSC+1) = 96MHz/1 = 96MHz
	
	TIM_BDTRStructInit(&TIM_BDTRInitStruct);
	TIM_BDTRInitStruct.TIM_OSSRState = TIM_OSSRState_Disable;
	TIM_BDTRInitStruct.TIM_OSSIState = TIM_OSSIState_Disable;
	TIM_BDTRInitStruct.TIM_LOCKLevel = TIM_LOCKLevel_2;    
	TIM_BDTRInitStruct.TIM_DeadTime  = dead_time;        
	
	TIM_BDTRInitStruct.TIM_Break     = TIM_Break_Enable;                    //刹车使能
	TIM_BDTRInitStruct.TIM_BreakPolarity   = TIM_BreakPolarity_High;        //刹车极性为高，高电平的时候刹车
	TIM_BDTRInitStruct.TIM_AutomaticOutput = TIM_AutomaticOutput_Enable;    //当刹车信号无效的时候自动开启输出
	TIM_BDTRConfig(TIM1, &TIM_BDTRInitStruct); 

	TIM_ARRPreloadConfig(TIM1, ENABLE);    		//使能自动重装载
    
    TIM_ITConfig(TIM1,TIM_IT_Break,DISABLE);	//关闭刹车中断
	TIM_ITConfig(TIM1,TIM_IT_Update,DISABLE);	//关闭更新中断

	NVIC_InitStructure.NVIC_IRQChannel = TIM1_BRK_UP_TRG_COM_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0x01;  //设置中断优先级
	NVIC_InitStructure.NVIC_IRQChannelCMD = DISABLE;     //中断使能
    NVIC_Init(&NVIC_InitStructure);
    
	TIM_CMD(TIM1, ENABLE);  //使能定时器1

}

void tim1_complementary_pwm_control(uint8 status)
{
    if(status)  
    {
        TIM_CtrlPWMOutputs(TIM1, ENABLE); 
    }
    else
    {
        TIM_CtrlPWMOutputs(TIM1, DISABLE); 
    }
}

