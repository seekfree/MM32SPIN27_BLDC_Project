/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2021,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：联系淘宝客服
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file            adc
 * @company         成都逐飞科技有限公司
 * @author          逐飞科技(QQ3184284598)
 * @version         查看doc内version文件 版本说明
 * @Software		IAR 8.32.4 or MDK 5.28
 * @Target core		MM32SPIN27PS
 * @Taobao          https://seekfree.taobao.com/
 * @date            2021-3-14
 ********************************************************************************************************************/
#ifndef _motor_H
#define _motor_H

#include "HAL_conf.h"

#include "zf_driver_pwm.h"
#include "zf_common_function.h"
#include "zf_driver_pwm.h"
#include "zf_driver_gpio.h"

#include "bldc_config.h"
#include "pid.h"
#include "pwm_input.h"
#include "move_filter.h"
#include "hall.h"
#include "adc.h"
#include "timer_pwm.h"
#include "led.h"



//定义电机转动速度输出引脚  电机越开引脚输出的波形频率越高 此引脚不可随意更改
#define MOTOR_SPEED_TIMER    	TIM_2
#define MOTOR_SPEED_OUT_PORT    TIM2_CH2_A12

//定义电机转动方向输出引脚      0：电机正转  1：电机反转   用于告知外部控制模块电机当前的运行方向
#define MOTOR_DIR_OUT_PORT      D2

//定义电机使能开关引脚          0：电机停止运行  1：电机根据输入信号正常运转
#define MOTOR_EN_STATUS_PORT    B14










//========PWM1 2 3通道输出使能位操作===================================== 
#define     PWMU_Enb            TIM1->CCER |= (uint16_t)( ((uint16_t)TIM_CCER_CC1E)) 
#define     PWMU_Dis            TIM1->CCER &= (uint16_t)(~((uint16_t)TIM_CCER_CC1E)) 
#define     PWMV_Enb            TIM1->CCER |= (uint16_t)( ((uint16_t)TIM_CCER_CC2E)) 
#define     PWMV_Dis            TIM1->CCER &= (uint16_t)(~((uint16_t)TIM_CCER_CC2E)) 
#define     PWMW_Enb            TIM1->CCER |= (uint16_t)( ((uint16_t)TIM_CCER_CC3E)) 
#define     PWMW_Dis            TIM1->CCER &= (uint16_t)(~((uint16_t)TIM_CCER_CC3E))  

#define     PWMUN_Enb           TIM1->CCER |= (uint16_t)( ((uint16_t)TIM_CCER_CC1NE)) 
#define     PWMUN_Dis           TIM1->CCER &= (uint16_t)(~((uint16_t)TIM_CCER_CC1NE)) 
#define     PWMVN_Enb           TIM1->CCER |= (uint16_t)( ((uint16_t)TIM_CCER_CC2NE)) 
#define     PWMVN_Dis           TIM1->CCER &= (uint16_t)(~((uint16_t)TIM_CCER_CC2NE)) 
#define     PWMWN_Enb           TIM1->CCER |= (uint16_t)( ((uint16_t)TIM_CCER_CC3NE)) 
#define     PWMWN_Dis           TIM1->CCER &= (uint16_t)(~((uint16_t)TIM_CCER_CC3NE))  


        
#define     PWMUVW_Enb          TIM1->CCER |= (uint16_t)(((uint16_t)(TIM_CCER_CC1E|TIM_CCER_CC2E|TIM_CCER_CC3E)))
#define     PWMUVW_Dis          TIM1->CCER &= (uint16_t)(~((uint16_t)(TIM_CCER_CC1E|TIM_CCER_CC2E|TIM_CCER_CC3E)))  
#define     PWMUVWN_Enb         TIM1->CCER |= (uint16_t)(((uint16_t)(TIM_CCER_CC1NE|TIM_CCER_CC2NE|TIM_CCER_CC3NE)))
#define     PWMUVWN_Dis         TIM1->CCER &= (uint16_t)(~((uint16_t)(TIM_CCER_CC1NE|TIM_CCER_CC2NE|TIM_CCER_CC3NE))) 

    
#define     PWMUH_ON_VL_ON		PWMUVW_Dis; PWMUVWN_Dis; PWMU_Enb; PWMUN_Enb; PWMV_Enb; PWMVN_Enb;
#define     PWMUH_ON_WL_ON		PWMUVW_Dis; PWMUVWN_Dis; PWMU_Enb; PWMUN_Enb; PWMW_Enb; PWMWN_Enb;
#define     PWMVH_ON_WL_ON  	PWMUVW_Dis; PWMUVWN_Dis; PWMW_Enb; PWMWN_Enb; PWMV_Enb; PWMVN_Enb;
#define     PWMVH_ON_UL_ON   	PWMUVW_Dis; PWMUVWN_Dis; PWMU_Enb; PWMUN_Enb; PWMV_Enb; PWMVN_Enb;
#define     PWMWH_ON_UL_ON   	PWMUVW_Dis; PWMUVWN_Dis; PWMU_Enb; PWMUN_Enb; PWMW_Enb; PWMWN_Enb;
#define     PWMWH_ON_VL_ON   	PWMUVW_Dis; PWMUVWN_Dis; PWMW_Enb; PWMWN_Enb; PWMV_Enb; PWMVN_Enb;
#define     PWMUVWH_OFF_UVWL_ON PWMUVW_Dis; PWMUVWN_Enb;


#define     PWMH_OFF_PWML_ON    PWMUVW_Dis; 	PWMUVWN_Enb;
#define     PWMH_ON_PWML_OFF    PWMUVWN_Dis;	PWMUVW_Enb;
#define     PWMH_OFF_PWML_OFF   PWMUVWN_Dis;	PWMUVW_Dis;





typedef enum
{
    FORWARD,    //正转
    REVERSE,    //反转
}MOTOR_DIR_enum;

typedef enum
{
    MOTOR_DISABLE,  //驱动关闭
    MOTOR_ENABLE,   //驱动使能
}MOTOR_EN_STATUS_enum;

typedef struct
{
    MOTOR_EN_STATUS_enum en_status; //指示电机使能状态
    uint8 brake_flag;   //指示当前刹车是否有效    1：正在刹车  0：正常运行    
    MOTOR_DIR_enum  dir;//期望电机旋转方向 FORWARD：正转  REVERSE：反转     BRAKE：刹车
    MOTOR_DIR_enum  dir_now;//当前电机旋转方向 FORWARD：正转  REVERSE：反转     BRAKE：刹车
    int32 set_speed;    //设置的速度
    int32 max_speed;    //速度最大值
    int32 min_speed;    //速度最小值
    int16 increase_step;//速度增加的步进值  加速的时候，值越大set_speed的值就会越快逼近except_speed
}motor_struct;


     



extern motor_struct motor_control;




void motor_speed_curve(void);
void motor_set_dir(void);
void motor_power_out(void);
void motor_commutation(uint8 except_hall);
void motor_speed_curve_init(void);

void tim2_pwm_frequency(uint16 frequency);
void motor_speed_out(void);
void motor_dir_out(void);
void motor_en(void);
void motor_information_out_init(void);



#endif
