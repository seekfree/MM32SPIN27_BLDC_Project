/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2021,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：联系淘宝客服
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file            adc
 * @company         成都逐飞科技有限公司
 * @author          逐飞科技(QQ3184284598)
 * @version         查看doc内version文件 版本说明
 * @Software		IAR 8.32.4 or MDK 5.28
 * @Target core		MM32SPIN27PS
 * @Taobao          https://seekfree.taobao.com/
 * @date            2021-3-14
 ********************************************************************************************************************/
#include "led.h"



void led_control(void)
{
    if(MOTOR_DISABLE == motor_control.en_status)
    {//电机已关闭
        led_en_control(LED_OFF);
        led_fault_control(LED_OFF);
        led_run_control(LED_OFF);
    }
    else
    {//电机已使能
        led_en_control(LED_ON);
        
        //检查刹车信号是否有效
        if(TIM_GetFlagStatus(TIM1, TIM_FLAG_Break) != RESET) 
        {
            TIM_ClearITPendingBit(TIM1, TIM_FLAG_Break);
            //当刹车信号有效的时候会运行这里的代码
            //例如点亮一个LED灯来指示
            led_fault_control(LED_ON);
        }
        else
        {
            led_fault_control(LED_OFF);
        }
        
        //检测是否在运转
        if(speed_filter.data_average)
        {
            led_run_control(LED_ON);   //开启运行灯
        }
        else
        {
            led_run_control(LED_OFF);   //关闭运行灯
        }
        
    }
}

void led_run_control(LED_STATUS_enum status)
{
	gpio_set(LED_RUN_PORT, (BitAction)status);
}


void led_fault_control(LED_STATUS_enum status)
{
	gpio_set(LED_FAULT_PORT, (BitAction)status);
}

void led_en_control(LED_STATUS_enum status)
{
	gpio_set(LED_EN_PORT, (BitAction)status);
}

void led_init(void)
{
	gpio_init(LED_RUN_PORT, GPO, 0, GPO_PUSH_PULL);
	gpio_init(LED_FAULT_PORT, GPO, 0, GPO_PUSH_PULL);
	gpio_init(LED_EN_PORT, GPO, 0, GPO_PUSH_PULL);
}