/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2021,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：联系淘宝客服
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file            adc
 * @company         成都逐飞科技有限公司
 * @author          逐飞科技(QQ3184284598)
 * @version         查看doc内version文件 版本说明
 * @Software		IAR 8.32.4 or MDK 5.28
 * @Target core		MM32SPIN27PS
 * @Taobao          https://seekfree.taobao.com/
 * @date            2021-3-14
 ********************************************************************************************************************/
#ifndef _LED_H
#define _LED_H	 

#include "hal_tim.h"

#include "zf_common_typedef.h"
#include "zf_driver_gpio.h"

#include "motor.h"
#include "move_filter.h"


#define LED_RUN_PORT        C15
#define LED_FAULT_PORT      C14
#define LED_EN_PORT         B13

typedef enum
{
    LED_ON,
    LED_OFF,
}LED_STATUS_enum;

void led_control(void);
void led_run_control(LED_STATUS_enum status);
void led_fault_control(LED_STATUS_enum status);
void led_en_control(LED_STATUS_enum status);
void led_init(void);


#endif
