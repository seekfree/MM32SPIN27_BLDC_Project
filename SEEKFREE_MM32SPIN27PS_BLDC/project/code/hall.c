/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2021,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：联系淘宝客服
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file            adc
 * @company         成都逐飞科技有限公司
 * @author          逐飞科技(QQ3184284598)
 * @version         查看doc内version文件 版本说明
 * @Software		IAR 8.32.4 or MDK 5.28
 * @Target core		MM32SPIN27PS
 * @Taobao          https://seekfree.taobao.com/
 * @date            2021-3-14
 ********************************************************************************************************************/

#include "hall.h"



uint8 hall_value_now;     //当前霍尔的值
uint8 hall_value_last;    //上次霍尔的值

uint8 next_hall_value;    //下一次霍尔的值

//霍尔硬件顺序为2 3 1 5 4 6
uint8 hall_steps_normal[2][8] = 
{
    //数据中的0并没有作用，只是用于占位
#if (2 == BLDC_MOTOR_TYPE)
    {0,2,4,6,1,3,5,0},  //正转的时候霍尔顺序
#else
    {0,3,6,2,5,1,4,0},  //正转的时候霍尔顺序
#endif
    {0,5,3,1,6,4,2,0},  //反转的时候霍尔顺序
};

uint8 hall_steps_advance[2][8] = 
{
    //数据中的0并没有作用，只是用于占位
    {0,2,4,6,1,3,5,0},  //正转的时候，超前换相霍尔顺序
    {0,4,1,5,2,6,3,0},  //反转的时候，超前换相霍尔顺序
};


uint8 hall_steps_break[8] = 
{
    //数据中的0并没有作用，只是用于占位
    0,1,2,3,4,5,6,0,  //正转的时候，超前换相霍尔顺序
};

uint8  hall_index = 0;
uint32 commutation_time_sum = 0;    //六次换相时间总和
uint16 commutation_time = COMMUTATION_TIMEOUT;    //统计本次换相所需时间
uint32 commutation_delay = COMMUTATION_TIMEOUT;   //延迟换相延时时间
uint16 commutation_delay_ratio = 1; //换相延时时间 = commutation_delay_ratio*commutation_time_sum>>7 数值越小则换相越超前


//-------------------------------------------------------------------------------------------------------------------
//  @brief      获取当前霍尔值
//  @param      void   
//  @return     void					
//  @since      
//-------------------------------------------------------------------------------------------------------------------
void read_hall_value(void)
{
	uint8 hall_a;
	uint8 hall_b;
	uint8 hall_c;
	
	hall_a = gpio_get(HALL_A_PIN);
	hall_b = gpio_get(HALL_B_PIN);
	hall_c = gpio_get(HALL_C_PIN);
	hall_value_now = hall_a*4 + hall_b*2 + hall_c; 
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      计算当前的速度
//  @param      void   
//  @return     void					
//  @since      
//-------------------------------------------------------------------------------------------------------------------
void calc_speed(void)
{
    //转速计算
    int32 speed;

    //电机转速计算说明
    //2.commutation_time是统计电机换相1次会进入多少次ADC中断
        //2.1 由于ADC是定时器1的PWM周期触发的，因为ADC中断频率与PWM周期是一样的频率40khz
        //2.2 函数调用关系ADC1_IRQHandler->scan_hall_status->calc_speed
        //2.3 commutation_time为统计换相时间的变量
    //3.通常电机转速我们都用RPM表示，RPM表示每分钟电机的转速
    //3.电机转一圈需要换相的次数等于 电机极对数*6
    //4.因此电机转速等于60*ADC中断频率/电机极对数/(commutation_time * 6)，这样可以得到电机每分钟的转速
    
    speed = ADC_NUM_PM/POLEPAIRS/(commutation_time * 6);
    
    if(REVERSE == motor_control.dir_now)//电机反转的时候需要对速度取反
    {
        speed = -speed;
    }

    move_filter_calc(&speed_filter, speed);
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      检查霍尔值
//  @param      void   
//  @return     void					
//  @since      检查霍尔值、调用速度计算函数、得出下次期望的霍尔值
//-------------------------------------------------------------------------------------------------------------------
void scan_hall_status(void)
{
	read_hall_value();
	
    commutation_time++;

    //换相超时保护
    //如果换相超过了250ms则认为出现堵转，应该及时将速度设置为0
    if(commutation_time >= COMMUTATION_TIMEOUT)		
	{
		commutation_time = 0;
		
        //滑动平均滤波初始化
        move_filter_init(&speed_filter);

        //输出的速度清零
        tim2_pwm_frequency(0);
	}
    
    if( (hall_value_now != hall_value_last) &&
        !commutation_delay)
    {
        
		//计算电机实际运行方向
		motor_direction_calculate();
		
        //转子速度
        calc_speed();
        
		//输出电机实际运行的方向信息
		motor_dir_out();
		
		//计时清空
		commutation_time = 0;
		
        //每次完成换相，速度输出
        motor_speed_out();
		
		if((speed_filter.data_average > 10000) || (speed_filter.data_average < -10000))
        {
            //速度大于一定的时候，霍尔延迟较大
            //因此采用超前换相加延时的方式去匹配最佳的换相点
            next_hall_value = hall_steps_advance[motor_control.dir][hall_value_now];
            commutation_delay = (commutation_delay_ratio*commutation_time_sum)>>7;
        }
        else
        {
            //速度较低无需超前换相
            next_hall_value = hall_steps_normal[motor_control.dir][hall_value_now];
            commutation_delay = 0;
        }
        
        hall_value_last = hall_value_now;
	}
}

void hall_init(void)
{
    gpio_init(HALL_A_PIN, GPI, 0, GPI_PULL_UP);
	gpio_init(HALL_B_PIN, GPI, 0, GPI_PULL_UP);
	gpio_init(HALL_C_PIN, GPI, 0, GPI_PULL_UP);
    //读取一下当前的霍尔值
    read_hall_value();
}

