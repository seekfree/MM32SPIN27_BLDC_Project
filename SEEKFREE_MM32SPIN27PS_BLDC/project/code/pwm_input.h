/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2021,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：联系淘宝客服
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file            adc
 * @company         成都逐飞科技有限公司
 * @author          逐飞科技(QQ3184284598)
 * @version         查看doc内version文件 版本说明
 * @Software		IAR 8.32.4 or MDK 5.28
 * @Target core		MM32SPIN27PS
 * @Taobao          https://seekfree.taobao.com/
 * @date            2021-3-14
 ********************************************************************************************************************/
#ifndef _pwm_input_h
#define _pwm_input_h


#include "hal_conf.h"
#include "zf_driver_gpio.h"




#define MOTOR_DIR_PORT  	C8
#define MOTOR_PWM_PORT_  	C9
#define MOTOR_PWM_PORT  	GPIOC
#define MOTOR_PWM_PIN   	GPIO_Pin_9

extern float duty_cycle;
extern uint8 duty_flag;
extern int16 period, duty_low, duty_high;



void pwm_input_init(void);
uint8 pwm_dir_get(void);

#endif
