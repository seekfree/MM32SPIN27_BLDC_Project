/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2021,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：联系淘宝客服
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file            adc
 * @company         成都逐飞科技有限公司
 * @author          逐飞科技(QQ3184284598)
 * @version         查看doc内version文件 版本说明
 * @Software		IAR 8.32.4 or MDK 5.28
 * @Target core		MM32SPIN27PS
 * @Taobao          https://seekfree.taobao.com/
 * @date            2021-3-14
 ********************************************************************************************************************/
#ifndef _pid_h
#define _pid_h


#include "zf_common_function.h"

#include "timer_pwm.h"
#include "motor.h"

typedef struct 
{
    float kp;           //PI闭环的比例参数
    float ki;           //PI闭环的积分参数
    int32 target_speed; //闭环的目标速度 不要直接修改这个变量
    int32 real_speed;   //当前电机转速
    int32 error;        //目标速度与实际速度误差
    int32 out_max;      //闭环最大输出
    int32 pout;         //闭环输出值
    float iout;         //闭环输出值
    int32 out;          //闭环输出值
}closed_loop_struct;



extern closed_loop_struct closed_loop;

extern void closed_loop_pi_init(void); //PI初始化程序
extern void closed_loop_pi_calc(int32 read_speed); //PI运算程序

#endif
