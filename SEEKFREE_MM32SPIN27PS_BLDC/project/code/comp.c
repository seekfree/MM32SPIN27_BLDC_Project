/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2021,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：联系淘宝客服
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file            adc
 * @company         成都逐飞科技有限公司
 * @author          逐飞科技(QQ3184284598)
 * @version         查看doc内version文件 版本说明
 * @Software		IAR 8.32.4 or MDK 5.28
 * @Target core		MM32SPIN27PS
 * @Taobao          https://seekfree.taobao.com/
 * @date            2021-3-14
 ********************************************************************************************************************/

#include "comp.h"

void comp_init(void)
{
    RCC_AHBPeriphClockCMD(RCC_AHBPeriph_GPIOD, ENABLE);
	
	
    GPIO_InitTypeDef  GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pin  =  GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;                           //引脚需要设置为模拟输入
	GPIO_Init(GPIOD, &GPIO_InitStructure);

    RCC_APB2PeriphClockCMD(RCC_APB2Periph_COMP, ENABLE);                    //开启时钟
    
    COMP_InitTypeDef COMP_InitStructure;
    COMP_InitStructure.COMP_InvertingInput = COMP_InvertingInput_CRV;       //比较器反向端连接到CRV
    COMP_InitStructure.COMP_NonInvertingInput = COMP_NonInvertingInput_IO0; //比较器同向端连接到IO0
    COMP_InitStructure.COMP_Output = COMP_Output_TIM1BKIN;                  //比较器输出到定时器1的刹车输入端口
    COMP_InitStructure.COMP_OutputPol = COMP_OutputPol_NonInverted;         //输出不反向
    COMP_InitStructure.COMP_Hysteresis = COMP_Hysteresis_No;                //比较器输出不延时
    COMP_InitStructure.COMP_Mode = COMP_Mode_MediumSpeed;                   //中等速率
    COMP_InitStructure.COMP_Filter = COMP_Filter_32_Period;                 //比较器输出滤波时长为64个周期
    
    SET_COMP_CRV(COMP_InvertingInput_CRV, BLDC_FAULT_THRESHOLD);            //设置CRV电压为2/20*VCC
    COMP_Init(COMP4, &COMP_InitStructure);
    COMP_CMD(COMP4, ENABLE);                                 //比较器使能
}

