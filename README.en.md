# 逐飞科技基于MM32SPIN27的智能车BLDC开源项目

#### Description
受组委会及灵动委托，逐飞科技针对第十七届智能汽车竞赛室外极速组的BLDC应用需求，精心设计了符合赛事需求的MM32 BLDC驱动开源方案供大家参考，该方案使用MM32SPIN27，此单片机具有3个运放及3个比较器，无需外加运放与比较器。电机为内转子、2860Kv、电压为12V。开源项目具有的功能，目前支持电机正反转、内部速度闭环、支持刹车、支持电机加速度可调、支持堵转保护。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
